import {all} from 'redux-saga/effects';

import {authSagas} from './auth';
import {carouselSagas} from './carousel';

function* sagas() {
  yield all([...authSagas, ...carouselSagas]);
}

export default sagas;
