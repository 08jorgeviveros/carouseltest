import {applyMiddleware, compose, createStore} from 'redux';

import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducer';
import sagas from './sagas';

//let store;

const IS_PRODUCTION = false;

function configureStore() {
  const sagaMiddlare = createSagaMiddleware();

  let middleware = applyMiddleware(sagaMiddlare);

  // config devtools
  if (!IS_PRODUCTION) {
    // @ts-ignore
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

    if (typeof devToolsExtension === 'function') {
      middleware = compose(middleware, devToolsExtension());
    }
  }

  const store = createStore(rootReducer, middleware);

  sagaMiddlare.run(sagas);

  return {
    store,
  };
}
export default configureStore;
