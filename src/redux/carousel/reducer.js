import {produce} from 'immer';
import {createReducer} from 'redux-act';

import {saveData} from './actions';
import {carouselInitialState} from './initial-state';

const carouselReducer = createReducer({}, carouselInitialState);

/** Reducer para guardar datos de carousel*/
carouselReducer.on(saveData, (state, payload) => {
  return produce(state, draftState => {
    draftState.data = payload.carousels;
  });
});

export {carouselReducer};
