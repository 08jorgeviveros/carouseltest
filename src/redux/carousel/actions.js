import {createAction} from 'redux-act';

/**
 *
 * Acciones para invocar sagas
 *
 * */

/** Obtiene data */
const getData = createAction('carousel/get data');

/** Flujo completo en una accion */
const getCompleteFlowData = createAction('carousel/get Complete flow data');

/***/

/**
 *
 * Acciones para actualizar store
 *
 * */

const saveData = createAction('carousel/save data');

export {getData, saveData, getCompleteFlowData};
