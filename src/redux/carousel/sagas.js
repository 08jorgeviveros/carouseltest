import {
  call,
  fork,
  put,
  takeEvery,
  putResolve,
  select,
  take
} from 'redux-saga/effects';

import api from '../../service';

import {getData, saveData, getCompleteFlowData} from './actions';
import {refreshToken} from '../auth/actions';
import {selectToken} from '../auth/selectors';

/**
 * Saga para obtener data de la app;
 */
function* getDataSaga() {
  try {
    const token = yield select(selectToken);

    if (token) {
      const result = yield call(api.getData, token);

      if (result.ok && result.data) {
        /** Agregar token a las cabeceras */
        saveData(result.data);

        /** Guardar token en store */
        yield put(saveData({carousels: result.data}));
      } else {
        yield put(refreshToken());
      }
    }

    // yield put(setAppLoading({ isLoading: false }));
  } catch (error) {
    // TODO: Error handler (error para mostrar modal)
    console.error('errosisimo',error);
  } finally {
    // yield put(setAppLoading({ isLoading: false }));
  }
}

/**
 * Saga para obtener data de la app;
 */
function* getCompleteFlowDataSaga() {
  try {
    yield putResolve(refreshToken());
    yield take('[3] auth/save token');


    yield putResolve(getData());

    // yield put(setAppLoading({ isLoading: false }));
  } catch (error) {
    // TODO: Error handler (error para mostrar modal)
    console.error(error);
  } finally {
    // yield put(setAppLoading({ isLoading: false }));
  }
}

function* watchGetData() {
  yield takeEvery(getData, getDataSaga);
}

function* watchGetCompleteFlowDataSaga() {
  yield takeEvery(getCompleteFlowData, getCompleteFlowDataSaga);
}

/**
 *
 * ROOT
 *
 */

const carouselSagas = [watchGetData, watchGetCompleteFlowDataSaga].map(fork);

export {carouselSagas};
