/**

 *Selector de token 
 * @param {RootState} state
 * @return {*}  token: string
 */
function selectToken(state) {
  return state?.auth?.token;
}

export {selectToken};
