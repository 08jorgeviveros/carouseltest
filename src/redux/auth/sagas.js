import {call, fork, put, takeLatest} from 'redux-saga/effects';

import api from '../../service';

import {setAuthToken} from '../../service/request';

import {refreshToken, saveToken} from './actions';

/**
 * Saga para refrescar token en app. El token vive cierto tiempo
 */
function* refreshTokenSaga() {
  try {
    const result = yield call(api.getToken);

    if (result.ok && result.data) {
      const {token} = result.data;

      /** Agregar token a las cabeceras */
      setAuthToken(token);

      /** Guardar token en store */
      yield put(saveToken({token}));
    }
  } catch (error) {
    // TODO: Error handler (error para mostrar modal)
    console.error(error);
  } finally {
    // yield put(setAppLoading({ isLoading: false }));
  }
}

function* watchRefreshToken() {
  yield takeLatest(refreshToken, refreshTokenSaga);
}

/**
 *
 * ROOT
 *
 */

const authSagas = [watchRefreshToken].map(fork);

export {authSagas};
