import {createAction} from 'redux-act';

/**
 *
 * Acciones para invocar sagas
 *
 * */

/** Refresca el token */
const refreshToken = createAction('auth/refreshToken');

/***/

/**
 *
 * Acciones para actualizar store
 *
 * */

const saveToken = createAction('auth/save token');

export {refreshToken, saveToken};
