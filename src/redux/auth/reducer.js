import {produce} from 'immer';
import {createReducer} from 'redux-act';

import {saveToken} from './actions';
import {authInitialState} from './initial-state';

const authReducer = createReducer({}, authInitialState);

/** Reducer para guardar datos de auth */
authReducer.on(saveToken, (state, payload) => {
  const {token} = payload;

  return produce(state, draftState => {
    draftState.token = token;
  });
});

export {authReducer};
