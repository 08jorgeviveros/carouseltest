import {combineReducers} from 'redux';

import {authReducer} from './auth';
import {carouselReducer} from './carousel/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  carousels: carouselReducer,
});

export default rootReducer;
