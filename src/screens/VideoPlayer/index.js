import React, {useRef} from 'react';
import {View, StyleSheet} from 'react-native';
import Video from 'react-native-video';

const VideoPlayer = ({route}) => {
  const {videoUrl} = route.params;
  const videoRef = useRef();
  const onBuffer = () => {
    console.log('call onBuffer');
  };
  const videoError = () => {
    console.log('call videoError');
  };
  return (
    <View style={styles.container}>
      <Video
        source={{uri: videoUrl}} // Can be a URL or a local file.
        ref={videoRef} // Store reference
        onBuffer={onBuffer} // Callback when remote video is buffering
        onError={videoError} // Callback when video cannot be loaded
        style={styles.backgroundVideo}
        resizeMode={'contain'}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundVideo: {
    width: '100%',
    height: 200,
  },
});

export default VideoPlayer;
