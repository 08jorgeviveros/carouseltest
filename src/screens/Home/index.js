import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Text, ScrollView} from 'react-native';
import {getCompleteFlowData} from '../../redux/carousel';
import Carousel from '../../components/Carousel';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const carousels = useSelector(state => state.carousels?.data);
  useEffect(() => {
    dispatch(getCompleteFlowData());
  }, [dispatch]);
  return (
    <ScrollView>
      {carousels.map(carousel => {
        return (
          <Carousel
            navigation={navigation}
            key={carousel.title}
            carouselData={carousel}
          />
        );
      })}
    </ScrollView>
  );
};

export default Home;
