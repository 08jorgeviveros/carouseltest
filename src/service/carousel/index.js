import request from '../request';

const endpoints = {
  data: 'data',
};

/**
 * Obtiene data
 * @return {*} Promise
 * @memberof Api
 */

async function getData(token) {
  try {
    request.setHeader('Authorization', `Bearer ${token}`);
    const response = await request.get(endpoints.data);

    if (response && response.data && response.status === 200) {
      return {
        ok: true,
        data: response.data,
      };
    }

    return {
      ok: false,
      message: 'error',
    };
  } catch (error) {
    return {
      ok: false,
      message: 'error',
    };
  }
}

const carouselApi = {
  getData,
};

export {carouselApi};
