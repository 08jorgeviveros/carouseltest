import {create} from 'apisauce';

import {API_URL} from '../config';

const request = create({
  baseURL: API_URL,
});

function setAuthToken(token) {
  request.setHeader('Authorization', `Bearer ${token}`);
}

export {request as default, setAuthToken};
