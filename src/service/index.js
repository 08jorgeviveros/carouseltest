import {authApi} from './auth';
import {carouselApi} from './carousel';

const api = {
  ...authApi,
  ...carouselApi,
};

export default api;
