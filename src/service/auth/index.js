import request, {setAuthToken} from '../request';

const endpoints = {
  authenticate: 'auth',
};

/**
 * Obtiene token
 * @return {*}  {Promise<GenericApiResponse<AuthenticateData>>}
 * @memberof Api
 */

async function getToken() {
  try {
    const response = await request.post(endpoints.authenticate, {
      sub: 'ToolboxMobileTest',
    });

    if (response && response.data) {
      const {token} = response.data;
      setAuthToken(token);
      return {
        ok: true,
        data: response.data,
      };
    }

    return {
      ok: false,
      message: 'error',
    };
  } catch (error) {
    return {
      ok: false,
      message: 'error',
    };
  }
}

const authApi = {
  getToken,
};

export {authApi};
