import React from 'react';
import {View, Text, FlatList, Dimensions, StyleSheet} from 'react-native';
import Poster from '../Poster';
import PropTypes from 'prop-types';

const {width, height} = Dimensions.get('window');

const Carousel = ({carouselData, navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{carouselData?.title}</Text>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={carouselData?.items}
        snapToInterval={
          carouselData?.type === 'poster' ? width * 0.5 + 20 : height * 0.5 + 20
        }
        bounces={false}
        decelerationRate={0}
        renderItem={({item}) => {
          return (
            <Poster
              navigation={navigation}
              data={item}
              type={carouselData?.type}
            />
          );
        }}
      />
    </View>
  );
};

Carousel.propTypes = {
  CarouselData: PropTypes.shape({
    items: PropTypes.array,
    type: PropTypes.string,
  }),
  navigation: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
  },
  title: {
    fontSize: 24,
  },
});

export default Carousel;
