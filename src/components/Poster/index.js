import React from 'react';
import {View, Text, Dimensions, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import PropTypes from 'prop-types';
const {width: windowWidth, height: windowHeight} = Dimensions.get('window');

const Poster = ({data, type, navigation}) => {
  return (
    <TouchableOpacity
      style={styles.containerThumb}
      onPress={() => {
        if (data?.videoUrl) {
          return navigation.navigate('VideoPlayer', {videoUrl: data.videoUrl});
        }
        alert('Video No Disponible');
      }}>
      <FastImage
        source={{uri: data.imageUrl, priority: FastImage.priority.normal}}
        style={
          type === 'poster'
            ? styles.imageBackgroundThumb
            : styles.imageBackgroundPoster
        }>
        {type === 'poster' && (
          <View style={styles.containerTitle}>
            <Text style={styles.title}>{data.title}</Text>
          </View>
        )}
      </FastImage>
      {type === 'thumb' && (
        <View>
          <Text style={styles.title}>{data.title}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
};
Poster.propTypes = {
  type: PropTypes.string,
  data: PropTypes.shape({
    imageUrl: PropTypes.string,
    videoUrl: PropTypes.string,
    title: PropTypes.string,
  }),
  navigation: PropTypes.object,
};

const styles = StyleSheet.create({
  containerThumb: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  imageBackgroundThumb: {
    width: windowWidth * 0.5,
    height: windowHeight * 0.5,
    justifyContent: 'flex-end',
  },
  imageBackgroundPoster: {
    height: windowWidth * 0.5,
    width: windowHeight * 0.5,
    justifyContent: 'flex-end',
  },
  containerTitle: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF4D',
    paddingVertical: 10,
  },
  title: {
    fontSize: 15,
    textAlign: 'center',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    borderWidth: 2,
  },
});

export default Poster;
