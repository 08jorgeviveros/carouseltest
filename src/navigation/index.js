import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screens/Home';
import VideoPlayer from '../screens/VideoPlayer';

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'Home'}>
        <Stack.Screen component={Home} name={'Home'} />
        <Stack.Screen component={VideoPlayer} name={'VideoPlayer'} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
