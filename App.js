/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {SafeAreaView, StatusBar, useColorScheme} from 'react-native';
import configureStore from './src/redux/store';
import {Provider} from 'react-redux';
import AppNavigator from './src/navigation';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const {store} = configureStore();

  return (
    <Provider store={store}>
      <SafeAreaView style={{flex:1}}>
        <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
        <AppNavigator />
      </SafeAreaView>
    </Provider>
  );
};

export default App;
